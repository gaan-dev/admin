import Vue from 'vue';

export default Vue.extend({
    data(){
        return {
            visible: false
        }
    },
    methods:{
        show(show = true){
            this.visible = show;
        },
        hide(){
            this.visible = false;
        }
    }
});

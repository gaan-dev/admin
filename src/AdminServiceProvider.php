<?php

namespace Gaan\Admin;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Gaan\Admin\Admin as LaravelAdmin;
use Gaan\Admin\CustomRouter;

class AdminServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->migrations();
        $this->config();
        $this->views();
        $this->resources();
        $this->register_commands();
    }

    protected function migrations()
    {
        $this->publishes([
            __DIR__.'/migrations/2018_06_24_113800_create_settings_table.php' => database_path('migrations/2018_06_24_113800_create_settings_table.php'),
        ]);
    }

    protected function resources()
    {
        $this->publishes([
            __DIR__.'/../resources/js' => resource_path('js/admin'),
            __DIR__.'/../resources/js/components' => resource_path('js/components/admin'),
            __DIR__.'/../resources/js/apps' => resource_path('js/apps'),
            __DIR__.'/../resources/js/routers' => resource_path('js/routers'),
            __DIR__.'/../resources/js/views' => resource_path('js/views'),
            __DIR__.'/../resources/scss' => resource_path('scss/admin')
        ], 'resources');
    }

    protected function config()
    {
        $this->publishes([
            __DIR__.'/config/admin.php' => config_path('admin.php'),
        ], 'config');
    }

    protected function views()
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'admin');
        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/admin'),
        ], 'views');
    }

    protected function register_commands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Gaan\Admin\Commands\InstallJavascript::class,
            ]);
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('admin', function ($app) {
            return new LaravelAdmin();
        });

        app()->config['filesystems.disks.models'] = [
            'driver' => 'local',
            'root'  =>  app_path(config('admin.model_directory'))
        ];
    }
}

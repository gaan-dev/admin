<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Admin Panel Prefix
    |--------------------------------------------------------------------------
    |
    | This prefix is used by the route generator to generate bespoke URLs
    | for the admin backend. This should not be left as blank or '/'
    |
    */

    'route_prefix' => 'admin',
    'webpack_location' => 'webpack.mix.js',
    'npm_package_location' => 'package.json',
    'javascript_import_root' => 'resources/js/',
    'javascript_import_targets' => [
        'apps' => 'apps/',
        'components' => 'components/admin/',
        'views' => 'views/admin/',
        'router' => 'routers/'
    ],
];

<?php

namespace Gaan\Admin;

use Route;
use Storage;

class Admin
{
    protected $config;

    public function __construct()
    {
        $this->config = config('admin');
    }

    public function routes($options = [])
    {
        $prefix = $this->config['route_prefix'];
        Route::get("{$prefix}/", '\Gaan\Admin\Controllers\AdminController@index')->name('admin.home');
    }
}

<?php

namespace Gaan\Admin\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use File;

// We should split this later on in to js and scss
class InstallJavascript extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:install:javascript';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Javascript to use VueRouter and scaffold out correctly.';

    /**
     * The config file for the package
     *
     * @var array
    */
    protected $paths;

    protected $root;

    protected $dependencies;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->config = config('admin');
        $this->root = $this->config['javascript_import_root'];
        $this->paths = $this->config['javascript_import_targets'];
        $this->dependencies = [
            'vue-router' => "^3.0.2",
            'material-design-icons' => "^3.0.1",
            "vue-clickaway" => "^2.2.2",
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->copyAdminApp()
            ->updatePackageDotJson()
            ->installDependencies()
            ->updateWebpackDotMixWithJs()
            ->updateWebpackDotMixWithScss();
        $this->info("Installation complete. You should run npm run dev && npm run dev now");
    }

    public function compile()
    {
        $process = new Process('npm run dev');
        $process->setTimeout(300);
        $process->run();
        return $this;
    }

    protected function copyAdminApp()
    {
        return $this->createFolders()
            ->copyApp()
            ->copyViews()
            ->copyRouter();
    }

    protected function updateWebpackDotMixWithJs()
    {
        $webpack = File::get($this->config['webpack_location']);
        $should_skip = strpos($webpack, "resources/js/apps/admin.js") !== false;
        if ($should_skip) {
            $this->info("Webpack already contains compilation rule for resources/js/apps/admin.js. Skipping");
            return $this;
        }
        $insert_point = strpos($webpack, ')', strpos($webpack, 'mix.js('));
        $split = [
            substr($webpack, 0, $insert_point+1),
            "\n   .js('resources/js/apps/admin.js', 'public/js')",
            substr($webpack, $insert_point+1)
        ];
        $this->info("Updating webpack");
        File::put($this->config['webpack_location'], implode($split));
        return $this;
    }

    protected function updateWebpackDotMixWithScss()
    {
        $webpack = File::get($this->config['webpack_location']);
        $should_skip = strpos($webpack, "resources/css/admin.css") !== false;
        if ($should_skip) {
            $this->info("Webpack already contains compilation rule for resources/css/admin.css. Skipping");
            return $this;
        }
        $insert_point = strpos($webpack, ')', strpos($webpack, '.postCss('));
        $split = [
            substr($webpack, 0, $insert_point+1),
            "\n   .postCss('resources/css/admin.css', 'public/css')",
            substr($webpack, $insert_point+1)
        ];
        $this->info("Updating webpack");
        File::put($this->config['webpack_location'], implode($split));
        return $this;
    }

    protected function installDependencies()
    {
        $this->info("Installing Dependencies");
        $process = new Process('npm install');
        $process->setTimeout(300);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return $this;
    }

    protected function updatePackageDotJson()
    {
        $package = $this->getPackageDotJson();
        if (!isset($package['dependencies'])) {
            $package['dependencies'] = [];
        }
        foreach ($this->dependencies as $dependency => $version) {
            $this->info("Adding {$dependency} ({$version}) to package.json");
            $package['dependencies'][$dependency] = $version;
        }
        File::put($this->config['npm_package_location'], json_encode($package));
        return $this;
    }

    protected function getPackageDotJson()
    {
        return json_decode(
            $this->cleanJson(
                File::get($this->config['npm_package_location'])
            ),
            true
        );
    }

    protected function cleanJson($string)
    {
        return str_replace(",", ",\n", $string);
    }

    protected function createFolders()
    {
        foreach ($this->paths as $suffix) {
            $path = $this->root . $suffix;
            try {
                File::makeDirectory($path, 0755, true);
                $this->info("Creating {$path}");
            } catch (\Exception $e) {
                $this->info($path . ' already exists. Skipping.');
            }
        }
        return $this;
    }

    protected function copyApp()
    {
        $this->root = config('admin.javascript_import_root');
        $this->info("Copying admin.js to {$this->root}{$this->paths['apps']}");
        File::copy(
            __DIR__.'/../resources/js/apps/admin.js',
            base_path($this->root . $this->paths['apps'] . 'admin.js')
        );
        return $this;
    }

    protected function copyViews()
    {
        $this->root = config('admin.javascript_import_root');
        $this->info("Copying views to {$this->root}{$this->paths['views']}");
        File::copyDirectory(
            __DIR__.'/../resources/js/views',
            base_path($this->root . $this->paths['views'])
        );
        return $this;
    }

    protected function copyComponents()
    {
        $this->info("Copying components to {$this->root}{$this->paths['components']}");
        File::copyDirectory(
            __DIR__.'/../resources/js/views',
            base_path($this->root . $this->paths['components'])
        );
        return $this;
    }

    protected function copyRouter()
    {
        $this->info("Copying router to {$this->root}{$this->paths['router']}");
        File::copy(
            __DIR__.'/../resources/js/router/admin.js',
            base_path($this->root . $this->paths['router'] . 'admin.js')
        );
        return $this;
    }
}
